Role Name
=========

A brief description of the role goes here.

Requirements
------------

Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required.

Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      tasks:
      - name: "import role lodspeakr"
      import_role:
        name: "lodspeakr"
      vars:
        lodspeakr_home: "/var/www/html/marcus-admin"
        lodspeakr_version: "master"
        lodspeakr_components_version: "master"
        lodspeakr_components_repo: "https://git.app.uib.no/uib-ub/spesialsamlingene/admin-components.git"
        lodspeakr_clone_user: "marcus"
        lodspeakr_endpoint_local: "localhost:3030/marcus-admin"
        lodspeakr_basedir: "https://{{ marcus_admin_servername }}/"
        lodspeakr_components_clone: false
        lodspeakr_typesAndValues: "array('crm:E22_Man-Made_Object' => -1, 'crm__E22_Man-Made_Object' => -1);"

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
